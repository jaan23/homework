package com.example.homework.service;


import com.example.homework.model.Person;
import com.example.homework.repository.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPersonServiceTest {

    @Mock
    private PersonRepository personRepositoryMock;

    @InjectMocks
    private DefaultPersonService defaultPersonService;

    @Test
    public void personWillNotBeUptatedIfFirstNameAndLastNameAlreadyExist() {
        Date randomDate = new Date();
        Person person1 = new Person(1, "John",
                "Mick",randomDate,"micky@mail.tv","Estonia" );
        Person person2 = new Person(2, "John",
                "Mick",randomDate,"micky@mail.tv","Estonia" );

        when(personRepositoryMock.getByFirstNameAndLastName(person2.getFirstName(),person2.getLastName()))
                .thenReturn(person1);

        defaultPersonService.updatePerson(person2);

        verify(personRepositoryMock, never()).save(person2);
    }

}
