package com.example.homework.service;

import com.example.homework.model.Person;

import java.util.List;

public interface PersonService {
    List<Person> getPeople();
    void addPerson(Person person);
    void updatePerson(Person person);
    void deletePerson(int id);

}
