package com.example.homework.service;

import com.example.homework.model.Person;
import com.example.homework.repository.PersonRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DefaultPersonService implements PersonService{

    @Resource
    PersonRepository personRepository;


    @Override
    public List<Person> getPeople() { return personRepository.findAll(); }

    @Override
    public void addPerson(Person person) {
        Person personWithSameName = personRepository.getByFirstNameAndLastName(person.getFirstName(),person.getLastName());
        if(personWithSameName == null){
            personRepository.saveAndFlush(person);
        }
    }

    @Override
    public void updatePerson(Person person) {
        Person personWithSameName = personRepository.getByFirstNameAndLastName(person.getFirstName(),person.getLastName());

        if(personWithSameName == null || (personWithSameName.getId()== person.getId())){
            personRepository.save(person);
        }
    }

    @Override
    public void deletePerson(int id) { personRepository.deleteById(id); }

}

