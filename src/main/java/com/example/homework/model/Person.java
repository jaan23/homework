package com.example.homework.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Size(min = 1, max = 60)
    @NotNull
    private String firstName;

    @Size(min = 1, max = 60)
    @NotNull
    private String lastName;

    @PastOrPresent
    @NotNull
    private Date birthDate;

    @Email
    @NotNull
    @Size(min = 3, max= 40)
    private String email;


    @Size(min = 3, max = 255)
    private String address;

}
