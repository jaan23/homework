package com.example.homework.repository;

import com.example.homework.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person getByFirstNameAndLastName(String firstName, String lastName);
}

