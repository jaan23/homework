package com.example.homework.controller;

import com.example.homework.model.Person;
import com.example.homework.service.PersonService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class PersonController {

    @Resource
    PersonService personService;

    @GetMapping("/person")
    public List<Person> getPeople(){
        return personService.getPeople();
    }

    @PostMapping("/person")
    @ResponseBody
    public void addPerson(@Valid @RequestBody Person person){
        personService.addPerson(person);
    }

    @PutMapping("/person")
    @ResponseBody
    public void updatePerson(@Valid @RequestBody Person person){
        personService.updatePerson(person);
    }

    @DeleteMapping("/person/{id}")
    @ResponseBody
    public void deletePerson(@PathVariable int id){
        personService.deletePerson(id);
    }
}
