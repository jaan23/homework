import React, { Component } from 'react';
import './App.css';
import { Button, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import Person from './Person';
import SearchBox from './SearchBox';
import moment from 'moment'


class PeopleList extends Component{
  constructor(props) {
    super(props);
    this.state = {
      people: [],
      sort: {
        column: null,
        direction: 'desc',
      }
    };
    this.remove = this.remove.bind(this);
    this.onSort = this.onSort.bind(this);
  }


  componentDidMount() {
    fetch('http://localhost:8080/api/person')
    .then(response => response.json())
    .then(data =>{
     this.setState({people: data})
     })
  }


  onSort = (column) =>{
    const direction = this.state.sort.direction === 'asc' ? 'desc' : 'asc';
    const sortedData = this.state.people.sort((a, b) => {
    if(a[column] < b[column])
      return -1;
    if(a[column] > b[column])
      return 1;
    return 0
    });

    if (direction === 'desc') {
      sortedData.reverse();
    }

    this.setState({
      people: sortedData,
      sort: {
        column,
        direction,
      }
    });
  }
  remove(id) {
    fetch('http://localhost:8080/api/person/'+id, {
      method: 'DELETE',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      }
    }).then(() => {
      let newList = this.state.people.filter(i => i.id !== id);
      this.setState({people: newList});
    });
  }

  formatDate(date){
    const newDate = moment(date, 'DD-MM-YYYY');
    return newDate;
  }


  render() {
    const peopleList = this.state.people.map(person =>
      <Person key={person.id} person={person} remove={this.remove}/>
	);

    return (
      <div>
        <div className="float-left">
          <SearchBox people ={this.state.people}/>
        </div>

	    <div className="float-right">
          <Button color="success" tag={Link} to="/people/new">Lisa uus isik</Button>
        </div>
	    <Table>
	      <tbody>
		    <tr>
		      <th onClick={() => this.onSort('firstName')}>Eesnimi</th>
		      <th onClick={() => this.onSort('lastName')}>Perenimi</th>
			  <th onClick={() => this.onSort('birthDate')}>Sünniaeg</th>
			  <th onClick={() => this.onSort('email')}>Email</th>
              <th onClick={() => this.onSort('address')}>Aadress</th>
              <th></th>
		    </tr>
		    {peopleList}
	      </tbody>
	    </Table>
	  </div>
	)
  }
}

export default PeopleList;