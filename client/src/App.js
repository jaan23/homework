import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PeopleList from './PeopleList';
import PersonEdit from './PersonEdit';

class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={PeopleList}/>
          <Route path='/people' exact={true} component={PeopleList}/>
          <Route path='/people/:id' component={PersonEdit}/>

        </Switch>
      </Router>
    )
  }
}

export default App;