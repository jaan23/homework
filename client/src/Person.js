import React, { Component } from 'react';
import './App.css';
import { Button, ButtonGroup} from 'reactstrap';
import { Link } from 'react-router-dom';
import moment from 'moment';

class Person extends Component{

  render() {
    const person = this.props.person
    return (
	  <tr>
	    <td>{person.firstName}</td>
          <td>{person.lastName}</td>
          <td>{moment(person.birthDate).format('YYYY-MM-DD')}</td>
          <td>{person.email}</td>
          <td>{person.address}</td>
          <td>
            <ButtonGroup>
              <Button size="sm" tag={Link} to={{ pathname: '/people/edit', state: {person: person} }}>Muuda</Button>
              <Button size="sm" onClick={() => this.props.remove(person.id)}>Kustuta</Button>
            </ButtonGroup>
          </td>
	  </tr>
	)
  }
}

export default Person;