import React, { Component } from 'react'

class SearchBox extends Component {
render() {
      let people = this.props.people;
      let firstNames = [...new Set(people.map(person => person.firstName))];
      let lastNames = [...new Set(people.map(person => person.lastName))];
      let addresses = [...new Set(people.map(person => person.address))];
      let searchValues = firstNames.concat(lastNames.concat(addresses))
      return (
      <div>
       <input type="text" list="data" onChange={this._onChange} />
       <datalist id="data">
         {searchValues.map((value) =>
            <option value={value}/>
          )}
        </datalist>

      </div>
  	  )
  }
}
export default SearchBox