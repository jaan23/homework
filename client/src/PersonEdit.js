import React, { Component } from 'react';
import './App.css';
import { Link,} from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import moment from 'moment';



class PersonEdit extends Component{

  emptyPerson = {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    address: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      person: this.emptyPerson,
      people: [],
      error: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.personNameError = this.personNameError.bind(this);
  }

  componentDidMount() {
    fetch('http://localhost:8080/api/person')
      .then(response => response.json())
      .then(data =>{
       this.setState({people: data})
       })

    if (this.props.match.params.id !== 'new') {
      this.setState(this.props.location.state);
    }
  }


  personNameError(){
    const{people} = this.state;
    const{person} = this.state;
    let result = people.find(p => {
      return p.firstName === person.firstName && p.lastName === person.lastName
    });
    if(result === undefined){
      this.setState({error:false})
    }else{
      if(result.id !== person.id){
        this.setState({error:true})
      }
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let person = this.state.person;
    person[name] = value;
    this.setState({person});
    this.personNameError();
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {person} = this.state;
    await fetch('http://localhost:8080/api/person', {
      method: (person.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(person),
    });
    this.props.history.push('/people');
  }

  render() {
    const {person} = this.state;
    const title = <h2>{person.id ? 'Muuda isik' : 'Lisa uus isik'}</h2>;
    const currentDate = moment().format('YYYY-MM-DD');

    return <div>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="firstName">Eesnimi</Label>
            <Input type="text" required="required" name="firstName" id="firstName" value={person.firstName}
                   pattern="^\S+$" onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="lastName">Perenimi</Label>
            <Input type="text" required="required" name="lastName" id="lastName" value={person.lastName}
                   pattern="^\S+$" onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="birthDate">Sünniaeg</Label>
            <Input type="date" required="required" name="birthDate" id="birthDate" value={moment(person.birthDate).format('YYYY-MM-DD')}
                   max={currentDate} onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="address">Aadress</Label>
            <Input type="text" required="required" name="address" id="address" value={person.address}
                   onChange={this.handleChange}/>
          </FormGroup>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input type="email" required="required" name="email" id="email" value={person.email}
                   onChange={this.handleChange}/>
            </FormGroup>
          <FormGroup>
            {!this.state.error &&
              <Button color="primary" type="submit">Save</Button>
            }
            <Button color="secondary" tag={Link} to="/people">Cancel</Button>
          </FormGroup>
        </Form>
        {this.state.error &&
          <i style={{color: "red", fontWeight: "bold"}}>Selline eesnimi ja perenimi on juba olemas!</i>
        }
      </Container>
    </div>
  }
}

export default PersonEdit;